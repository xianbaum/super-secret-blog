# Super Secret Dev Blog Software

## Overview

This is a very basic blog software I wrote in Java in early 2017.

There is really not much to say about it.

I developed it to run on an original Raspberry Pi and it is indeed capable. It is very simple and I do not currently plan to develop this further.

## Building

Maven is required to build this. It can be built with the following command:

`mvn package`

## Setup

I hard-coded some things, so you will need to change some values.

1. On DevBlogConnectionFactory.java, chagne the String user and String password to your connection username and password.
2. On the ApiController, secretPostPassword should be changed to a password, unless you want strangers posting to your blog.
3. You will want to change the title, at the very least in blogmain.jsp to not say Christian Michael Baum.

On the postgresql database, you can setup with these statements pasted into the shell:

```sql
CREATE DATABASE xianbaumdevblog;
\c xianbaumsecretblog;
CREATE TABLE post (
 id SERIAL PRIMARY KEY NOT NULL,
 title VARCHAR(255),
 body TEXT NOT NULL,
 date_created TIMESTAMP DEFAULT current_timestamp
);
```

## Usage

You can post to the blog by using /api/post with body and optionally/nullable title. Make sure to set secretPostPassword or else anyone can post to your blog.

## Wow

I wrote a lot for something I plan to rarely use and never update!
