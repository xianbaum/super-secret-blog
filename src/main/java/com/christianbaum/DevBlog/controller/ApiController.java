package com.christianbaum.DevBlog.controller;
import java.sql.SQLException;

import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.*;
import com.christianbaum.DevBlog.models.BlogPostModel;
import com.christianbaum.DevBlog.servicemodels.BlogPostServiceModel;

@RestController
public class ApiController {
	static final String secretPostPassword = "pass";
	@RequestMapping(value = "/api/post", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public String post(@RequestBody BlogPostModel post) throws ClassNotFoundException, IllegalStateException, SQLException {
                if(secretPostPassword.equals(post.getPassword())) {
                	return Integer.toString(BlogPostServiceModel.createBlogPost(post));
                }
                return "no!";
	}

	@RequestMapping(value = "/api/getposts", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public String getPosts() throws JsonProcessingException, ClassNotFoundException, SQLException {
		return new ObjectMapper().writeValueAsString(BlogPostServiceModel.getAllBlogPosts());
	}
	
	@RequestMapping(value = "/api/getpost/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public String getPostById(@PathVariable(value="id")int id) throws JsonProcessingException, IllegalArgumentException, ClassNotFoundException, SQLException {
		return new ObjectMapper().writeValueAsString(BlogPostServiceModel.getBlogPostByID(id));
	}
}
