package com.christianbaum.DevBlog.controller;
 
import java.sql.SQLException;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.christianbaum.DevBlog.servicemodels.BlogPostServiceModel;
 
@Controller
public class HomeController {
	@RequestMapping(value={"/", "", "index.html", "/index"})
	public ModelAndView PostList() throws ClassNotFoundException, SQLException {
		return new ModelAndView("blogmain", "model", BlogPostServiceModel.getAllBlogPosts());
	}
	
	@RequestMapping("/post/{id}")
	public ModelAndView Post(@PathVariable("id") int id) throws ClassNotFoundException, SQLException {
		return new ModelAndView("blogpost", "post", BlogPostServiceModel.getBlogPostByID(id));
	}
}
