package com.christianbaum.DevBlog.models;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonCreator;

public class BlogPostModel {
	int id;
	String body;
	Date dateCreated;
	String password;
	String title;
	@JsonCreator
	public BlogPostModel() {
		
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}
	public Date getDateCreated() {
		return dateCreated;
	}
	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}
    	public String getPassword() {
		return password;
	}
    	public void setPassword(String password) {
		this.password = password;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
}
