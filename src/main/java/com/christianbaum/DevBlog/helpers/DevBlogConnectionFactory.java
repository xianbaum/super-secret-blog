package com.christianbaum.DevBlog.helpers;

import java.sql.*;
import java.util.*;

final public class DevBlogConnectionFactory {
	static final String url = "jdbc:postgresql://localhost/xianbaumdevblog";
	static final String user = "user";
	static final String password = "pass";
	private DevBlogConnectionFactory(){
		
	}
	public static Connection create() throws ClassNotFoundException, SQLException  {
		Class.forName("org.postgresql.Driver");
		Properties properties = new Properties();
		properties.setProperty("user", user);
		properties.setProperty("password", password);
		return DriverManager.getConnection(url, properties);
	}
}
