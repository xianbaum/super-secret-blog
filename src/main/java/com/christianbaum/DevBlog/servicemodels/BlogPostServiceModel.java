package com.christianbaum.DevBlog.servicemodels;

import java.sql.*;
import java.util.*;

import com.christianbaum.DevBlog.helpers.DevBlogConnectionFactory;
import com.christianbaum.DevBlog.models.BlogPostModel;

final public class BlogPostServiceModel {
	private BlogPostServiceModel(){
	}
	private static BlogPostModel blogPostFromResultString(ResultSet rs) throws SQLException{
		BlogPostModel post = new BlogPostModel();
		post.setBody(rs.getString("body"));
		post.setDateCreated(rs.getDate("date_created"));
		post.setId(rs.getInt("id"));
		post.setTitle(rs.getString("title"));
		return post;
	}
	public static BlogPostModel getBlogPostByID(int id) throws IllegalArgumentException, SQLException, ClassNotFoundException {
		final String query = "select id, title, body, date_created from post where id = ?";
		Connection con = DevBlogConnectionFactory.create();
		PreparedStatement statement = con.prepareStatement(query);
		try {
			statement.setInt(1, id);
			ResultSet rs = statement.executeQuery();
			if(rs.next()) {
				return blogPostFromResultString(rs);
			} else {
				throw new IllegalArgumentException(String.format("The ID %d returned an empty set.", id));
			}
		}
		finally {
			statement.close();
			con.close();
		}
	}
	public static List<BlogPostModel> getAllBlogPosts() throws SQLException, ClassNotFoundException {
		final String query = "select id, title, body, date_created from post order by date_created desc";
		Connection con = DevBlogConnectionFactory.create();
		PreparedStatement statement = con.prepareStatement(query);
		try{
			ResultSet rs = statement.executeQuery();
			List<BlogPostModel> posts = new  ArrayList<BlogPostModel>();
			while(rs.next()) {
				posts.add(blogPostFromResultString(rs));
			}
			return posts;
		}
		finally {
			statement.close();
			con.close();
		}
	}
	public static int createBlogPost(BlogPostModel post) throws ClassNotFoundException, SQLException, IllegalStateException {
		final String query = "insert into post (title, body) values (?, ?) returning id";
		Connection con = DevBlogConnectionFactory.create();
		PreparedStatement statement = con.prepareStatement(query);
		try {
			statement.setString(1, post.getTitle());
			statement.setString(2, post.getBody());
			ResultSet rs = statement.executeQuery();
			if(rs.next()) {
				return rs.getInt("id");
			} else {
				throw new IllegalStateException("The create procedure did not return an ID");
			}
		}
		finally {
			statement.close();
			con.close();
		}
	}
}
