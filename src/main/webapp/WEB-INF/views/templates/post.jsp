<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div>
	<c:if test="${post.title != null}"><h2>${post.title}</h2></c:if>
	<div>${post.body}</div>
	<footer>
		<div class="footer">
			Posted on ${post.dateCreated}
			<a href="${pageContext.servletContext.contextPath}/post/${post.id}">Permalink</a>
		</div>
	</footer>
</div>