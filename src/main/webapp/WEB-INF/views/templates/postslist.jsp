<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:forEach items="${model}" var="post">
<div>
	<c:if test="${post.title != null}"><h2>${post.title}</h2></c:if>
	<div>${post.body}</div>
	<footer>
		<div class="footer">
			Posted on ${post.dateCreated}
			<a href="${pageContext.servletContext.contextPath}/post/${post.id}">See More</a>
		</div>
	</footer>
</div>
</c:forEach>