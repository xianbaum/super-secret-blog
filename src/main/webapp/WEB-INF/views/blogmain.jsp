<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@  taglib  prefix="c"   uri="http://java.sun.com/jsp/jstl/core"  %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Christian Michael Baum</title>
	<link rel="stylesheet" href="${pageContext.servletContext.contextPath}/webjars/bootstrap/3.3.7-1/css/bootstrap.min.css">
	<link rel="stylesheet" href="${pageContext.servletContext.contextPath}/resources/styles/blogapp.css">
</head>
<body>
<h1><a href="${pageContext.servletContext.contextPath}/" id="h1">Christian Michael Baum</a></h1>
<div id="content">
	<article>
		<tiles:insertAttribute name="body" />
	</article>
</div>
</body>
</html>